module Main where

import           Data.String.Strip
import           Data.List ((\\))
import           Control.Monad (guard)
import           Control.Monad.Primitive
import           Control.Monad.ST     (ST, runST)
import           Data.Graph.Inductive (Gr, labEdges, match, matchAny, nodes,
                                       order, (&))
import qualified Data.Graph.Inductive as Graph
import           Data.List            (sortBy)
import qualified Data.Vector          as V
import qualified Data.Vector.Mutable  as VM
import           Debug.Trace          (trace)
import qualified System.Random.MWC as Rand

import Control.Monad.Trans.State (StateT, runStateT, evalStateT)
import Control.Monad.State.Class (MonadState, get)
import Control.Monad.IO.Class (MonadIO, liftIO)

import Data.Array.ST (STUArray, newArray, newListArray, readArray, writeArray)

import Diagrams.Prelude
import Diagrams.Backend.Rasterific
import Diagrams.Backend.Rasterific.CmdLine

-- graph algos
gHead :: Gr n e -> Maybe (Graph.Node, Gr n e)
gHead g | Graph.isEmpty g = Nothing
gHead (matchAny -> ((_, n, _, _), g)) = Just (n, g)

gMapNodes :: (n -> n') -> Gr n e -> Gr n' e
gMapNodes _ (Graph.isEmpty -> True) = Graph.empty
gMapNodes f (matchAny -> ((in', n, l, out'), g)) = (in', n, f l, out') Graph.& gMapNodes f g

gDfs :: Graph.Node -> Gr n e -> [Graph.Node]
gDfs s graph = go graph
  where
    go (Graph.isEmpty -> True) = []
    go (match s -> (Nothing, _)) = []
    go (match s -> (Just _, g)) = dfs [s] g

    dfs (n:ns) (match n -> (Just c, g)) = n : dfs (Graph.neighbors' c ++ ns) g
    dfs (n:ns) (match n -> (Nothing, g)) = dfs ns g

gEDfs :: Graph.Node -> Gr n e -> [Graph.LEdge e]
gEDfs _ (Graph.isEmpty -> True) = []
gEDfs s (match s -> (Nothing, _)) = []
gEDfs s (match s -> (Just c, graph)) = go (lNeighbors' c) graph
  where
    go [] _ = []
    go ((n, next, l):ens) (match next -> (Just c, g)) = (n, next, l) : go (lNeighbors' c ++ ens) g
    go (_:ens) g = go ens g

    lNeighbors' (in', n, l, out') = [(n, n', l') | (l', n') <- in' ++ out']

-- | A version of gEDfs where the order neighbors are visited randomly
gEDfsR :: (PrimMonad m, MonadState (Rand.Gen (PrimState m)) m) => (forall t . [t] -> m [t]) -> Graph.Node -> Gr n e -> m [Graph.LEdge e]
gEDfsR shffl _ (Graph.isEmpty -> True) = return []
gEDfsR shffl s (match s -> (Nothing, _)) = return []
gEDfsR shffl s (match s -> (Just c, graph)) = go (lNeighbors' c) graph
  where
    lNeighbors' (in', n, l, out') =[(n, n', l') | (l', n') <- out']
    go [] _ = return []
    go ((n, next, l):ens) (match next -> (Just c, g)) = do
      edges <- shffl (lNeighbors' c)
      ((n, next, l) :) <$> go (edges ++ ens) g
    go (_:ens) g = go ens g

shuffle :: forall m t . (PrimMonad m, MonadState (Rand.Gen (PrimState m)) m) => [t] -> m [t]
shuffle [] = return []
shuffle ls = V.toList <$> (swaps (length ls) (V.fromList ls))
  where
    swaps :: (PrimMonad m) => Int -> V.Vector t -> m (V.Vector t)
    swaps 1 v =  return v
    swaps n v = do
      let
        swap :: Int -> Int -> V.MVector s t -> ST s ()
        swap a b v = do
          ta <- VM.read v a
          tb <- VM.read v b
          VM.write v a tb
          VM.write v b ta

      rng <- get
      i <- Rand.uniformR (0, n-1) rng
      swaps (n - 1) $ V.modify (swap (n-1) i) v

newtype Rn r m a = Rn { unRn :: StateT r m a }
  deriving (Functor, Applicative, Monad, MonadState r)

-- gEDfsR shuffle 0 ((0, "0") `Graph.insNode` Graph.empty)

-- union find algo
data UnionFind s where
  UnionFind :: { ids :: STUArray s Int Int
               , szs :: STUArray s Int Int
               }
            -> UnionFind s

newUnionFind :: Int -> ST s (UnionFind s)
newUnionFind n = UnionFind <$> (newListArray (0, n-1) [0..n-1]) <*> (newArray (0, n-1) 1)

find :: UnionFind s -> Int -> Int -> ST s Bool
find uf a b = (==) <$> (root uf a) <*> (root uf b)

root :: UnionFind s -> Int -> ST s Int
root uf a = do
  a' <- readArray (ids uf) a
  if a' /= a
    then (do
             pa' <- readArray (ids uf) a'
             writeArray (ids uf) a a'
             root uf pa')
    else return a'

unite :: UnionFind s -> Int -> Int -> ST s ()
unite uf a b = do
  a' <- root uf a
  b' <- root uf b
  let
    akt = do
      sza <- readArray (szs uf) a'
      szb <- readArray (szs uf) b'
      if sza < szb
        then (do
                 writeArray (ids uf) a' b'
                 writeArray (szs uf) b' (sza + szb))
        else (do  writeArray (ids uf) b' a'
                  writeArray (szs uf) a' (szb + sza))

    nothin = return ()

  if a' == b'
    then nothin
    else akt

-- wall
data Wall where
  Horizontal :: (Int, Int) -> Wall
  Vertical :: (Int, Int) -> Wall
  deriving (Eq, Show)

data Weighted a where
  W :: Int -> a -> Weighted a
  deriving (Eq, Show)

instance Eq a => Ord (Weighted a) where
  compare (W i _) (W i'' _) = compare i i''

-- grid
type GridT n = Gr n Wall
type Grid = GridT ()

grid :: Int -> Int -> Grid
grid = undefined

wGrid :: forall m . (PrimMonad m, MonadState (Rand.Gen (PrimState m)) m) => Int -> Int -> (Int, Int) -> m (Gr () (Weighted Wall))
wGrid w h wrange = Graph.mkGraph <$> nodes <*> edges
  where
    pos :: Int -> (Int, Int)
    pos = flip divMod w
      where
        swap (x,y) = (y,x)

    edges :: m [Graph.LEdge (Weighted Wall)]
    edges = do
      rng <- get
      ns <- nodes
      let
        vs wght = do -- list (vertical)
          (n, _) <- ns
          guard $ (snd . pos) n /= w-1
          return (n, n+1, W wght (Vertical (pos n)))

        hs wght = do -- list (horizontal)
          (n, _) <- ns
          guard $ (fst . pos) n < h
          return (n, n+w, W wght (Horizontal (pos n)))

      (++) <$> (vs <$> Rand.uniformR wrange rng) <*> (hs <$> Rand.uniformR wrange rng)

    nodes :: m [Graph.LNode ()]
    nodes = return $ do -- list
      n <- [0..w*h -1]
      return (n, ())

maze :: forall m . (PrimMonad m, MonadState (Rand.Gen (PrimState m)) m) => Int -> Int -> (Int, Int) -> m [(Weighted Wall)]
maze w h wrange = do
  grd <- wGrid w h wrange
  rWalls <- (Graph.edgeLabel <$>) <$> gEDfsR (return) 0 grd
  return $ (Graph.edgeLabel <$> Graph.labEdges grd) \\ rWalls

main :: IO ()
main = do
  let
    h = 10
    w = 10
  walls <- Rand.create >>= evalStateT (maze w h (1,10))
  mainWith (rnder w h walls)
  where
    rnder :: Int -> Int -> [Weighted Wall] -> Diagram B
    rnder w h walls = mconcat $ do
      W w wall <- walls
      return $ paint wall w

    paint :: Wall -> Int -> QDiagram B V2 Double Any
    paint (Horizontal p@(x, y)) w = position [(p2 (fromIntegral y, fromIntegral x), (text.show) p # scale 0.2 === (circle 0.1 ||| hrule (0.8) ||| circle 0.1))]
    paint (Vertical p@(x, y)) w = position [(p2 (fromIntegral y, fromIntegral x), (circle 0.1 === vrule (0.8) === circle 0.1) |||  (text.show) p # scale 0.2)]
